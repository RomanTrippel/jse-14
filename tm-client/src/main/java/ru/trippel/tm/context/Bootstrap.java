package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.api.service.IStateService;
import ru.trippel.tm.api.service.ITerminalService;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.endpoint.*;
import ru.trippel.tm.service.StateService;
import ru.trippel.tm.service.TerminalService;
import ru.trippel.tm.util.PackageClassFinderUtil;
import ru.trippel.tm.view.BootstrapView;
import java.lang.Exception;
import java.text.ParseException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final IStateService stateService = new StateService(commands);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpointService().getDataEndpointPort();

    public void start() throws Exception {
        BootstrapView.printWelcome();
        @NotNull String command = "";
        init();
        while (true) {
            try {
                command = terminalService.read();
                if ("EXIT".equals(command)) {
                    BootstrapView.printGoodbye();
                    break;
                }
                if (command.isEmpty()) {
                    BootstrapView.printErrorCommand();
                    continue;
                }
                execute(getCommand(command));
            }
            catch (@NotNull final NumberFormatException | ParseException e) {
                BootstrapView.printIncorrectlyData();
            } catch (@NotNull final Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(@Nullable final AbstractCommand command) throws Exception {
        if (command == null) return;
        @Nullable final AbstractCommand commandTMP = checkPermission(command);
        if (commandTMP == null) return;
            commandTMP.execute();
    }

    @Nullable
    private AbstractCommand getCommand(@Nullable final String command) {
        if (command == null) {
            return null;
        }
        if (command.isEmpty()) {
            return null;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) BootstrapView.printErrorCommand();
        return abstractCommand;
    }

    @Nullable
    private AbstractCommand checkPermission(@Nullable final AbstractCommand command) {
        if (command == null) return null;
        if (command.secure()) return command;
        if (stateService.getToken() == null) {
            System.out.println("You must log in first.");
            return null;
        }
        return command;
    }

    public void init() throws InstantiationException, IllegalAccessException {
        registerCommands();
    }

    private void registerCommands() throws IllegalAccessException, InstantiationException {
        @NotNull AbstractCommand command;
        @NotNull final String pathCommandPackage = "ru.trippel.tm.command";
        @NotNull final Set<Class<? extends AbstractCommand>> allClasses = PackageClassFinderUtil.
                getAllClasses(pathCommandPackage);
        for (@NotNull final Class<? extends AbstractCommand> abstractCommand: allClasses) {
            command = abstractCommand.newInstance();
            command.setServiceLocator(serviceLocator);
            commands.put(command.getNameCommand(), command);
        }
    }

}