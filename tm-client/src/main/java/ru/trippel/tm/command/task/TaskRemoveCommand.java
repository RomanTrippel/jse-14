package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.TaskDTO;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task remove";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove a task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint().findAllTasksByUserId(token);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Delete:");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        serviceLocator.getTaskEndpoint().removeTask(token, taskId);
        System.out.println("The task has been deleted.");
    }

}
