package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.UserDTO;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.PasswordHashUtil;

@NoArgsConstructor
public final class UserPassChangeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user pass change";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final UserDTO userTemp = serviceLocator.getUserEndpoint().findOneUser(token);
        if (userTemp == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Password.");
        @NotNull final String passwordNew = serviceLocator.getTerminalService().read();
        @Nullable final String hashPasswordNew = PasswordHashUtil.getHash(passwordNew);
        userTemp.setPassword(hashPasswordNew);
        serviceLocator.getUserEndpoint().updateUser(token, userTemp);
        serviceLocator.getSessionEndpoint().removeSession(token);
        serviceLocator.getStateService().setToken(null);
        System.out.println("Changes applied. Login required again.");
    }

}
