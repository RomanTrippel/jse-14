package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.TaskDTO;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.TaskPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskFindByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task find by part";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a task by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        System.out.println("Enter the part of the name or description to search.");
        @NotNull final String searchText = serviceLocator.getTerminalService().read();
        @Nullable final List<TaskDTO> foundTaskList = serviceLocator.getTaskEndpoint()
                .findTaskByPart(token, searchText);
        System.out.println("List of found tasks:");
        if (foundTaskList != null) {
            if (foundTaskList.size() != 0) {
                for (@NotNull final TaskDTO task : foundTaskList) {
                    System.out.println(TaskPrintUtil.print(task));
                }
            }
        }
        System.out.println("Search for matches is over.");
    }

}
