package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "login";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Authorization, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        if ( serviceLocator.getStateService().getToken() != null) {
            @Nullable final String token = serviceLocator.getStateService().getToken();
            serviceLocator.getSessionEndpoint().removeSession(token);
            serviceLocator.getStateService().setToken(null);
        }
        System.out.println("Enter login:");
        @NotNull final String login = serviceLocator.getTerminalService().read();
        System.out.println("Enter password:");
        @NotNull final String password = serviceLocator.getTerminalService().read();
        @Nullable final String token = serviceLocator.getSessionEndpoint().createSession(login, password);
        serviceLocator.getStateService().setToken(token);
        if (serviceLocator.getStateService().getToken() == null){
            System.out.println("You entered the login or password incorrectly.");
        }
        System.out.println(login + ", you are logged in.");
    }

}
