package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.ProjectDTO;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project create";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        System.out.println("Enter project name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        if (name.isEmpty()) {
            System.out.println("An empty name is entered. Try it again.");
        }
        else {
            @NotNull final ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setName(name);
            serviceLocator.getProjectEndpoint().createProject(token, projectDTO);
            System.out.println("The project \"" + name + "\" added!");
        }
    }

}
