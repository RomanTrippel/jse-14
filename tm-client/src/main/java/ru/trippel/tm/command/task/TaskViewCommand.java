package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.TaskDTO;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.TaskPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null) return;
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint().findAllTasksBySort(token);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Task List:");
        @Nullable TaskDTO taskDTO;
        for (int i = 0; i < taskList.size(); i++) {
            taskDTO = taskList.get(i);
            System.out.println(i+1 + ". " + TaskPrintUtil.print(taskDTO));
        }
    }

}
