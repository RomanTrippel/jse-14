package ru.trippel.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataFasterxmlXmlSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml xml save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data as XML using FasterXML.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        serviceLocator.getDataEndpoint().dataFasterxmlXmlSave(token);
        System.out.println("Data saved.");
    }

}
