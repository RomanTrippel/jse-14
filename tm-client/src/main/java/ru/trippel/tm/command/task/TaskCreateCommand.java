package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.ProjectDTO;
import ru.trippel.tm.api.endpoint.TaskDTO;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task create";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpoint()
                .findAllProjectsByUserId(token);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("Project List is empty.\nTo create a Task, you must have at least one Project.");
            return;
        }
        int projectNum = -1;
        System.out.println("Projects List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        System.out.println("Enter task name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        if (name.isEmpty()) {
            System.out.println("An empty name is entered. Try it again.");
        }
        else {
            @NotNull final TaskDTO taskDTO = new TaskDTO();
            taskDTO.setName(name);
            taskDTO.setProjectId(projectId);
            serviceLocator.getTaskEndpoint().createTask(token, taskDTO);
            System.out.println("The task \"" + name + "\" added!");
        }
    }

}
