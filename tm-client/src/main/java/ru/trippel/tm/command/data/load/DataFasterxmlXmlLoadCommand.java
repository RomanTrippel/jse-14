package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.IOException_Exception;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataFasterxmlXmlLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml xml load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data as XML using FasterXML.";
    }

    @Override
    public void execute() throws Exception_Exception, IOException_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        serviceLocator.getDataEndpoint().dataFasterxmlXmlLoad(token);
        serviceLocator.getSessionEndpoint().removeSession(token);
        serviceLocator.getStateService().setToken(null);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
