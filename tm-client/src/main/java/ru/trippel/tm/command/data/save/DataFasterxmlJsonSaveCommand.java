package ru.trippel.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.IOException_Exception;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.command.AbstractCommand;

import java.io.IOException;

@NoArgsConstructor
public final class DataFasterxmlJsonSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml json save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data as Json using FasterXML.";
    }

    @Override
    public void execute() throws IOException, Exception_Exception, IOException_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        serviceLocator.getDataEndpoint().dataFasterxmlJsonSave(token);
        System.out.println("Data saved.");
    }

}
