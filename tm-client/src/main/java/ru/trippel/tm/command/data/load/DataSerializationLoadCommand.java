package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataSerializationLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data ser load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data.";
    }

    @Override
    public void execute() throws Exception_Exception, ClassNotFoundException_Exception, IOException_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        serviceLocator.getDataEndpoint().dataSerializationLoad(token);
        serviceLocator.getSessionEndpoint().removeSession(token);
        serviceLocator.getStateService().setToken(null);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}
