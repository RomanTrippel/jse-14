package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.UserDTO;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user view profile";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final UserDTO user = serviceLocator.getUserEndpoint().findOneUser(token);
        if (user == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.printf(
                "%s - %s - %s - %s - %s\n"
                , user.getId(), user.getLoginName(), user.getPassword()
                , user.getProjectSortingMethod(), user.getTaskSortingMethod()
        );
    }

}
