package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.ProjectDTO;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.ProjectPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class ProjectViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null) return;
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpoint().findAllProjectsBySort(token);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Project List:");
        @Nullable ProjectDTO projectDTO;
        for (int i = 0; i < projectList.size(); i++) {
            projectDTO = projectList.get(i);
            System.out.println(i+1 + ". " + ProjectPrintUtil.print(projectDTO));
        }
    }

}