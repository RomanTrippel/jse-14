package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.ProjectDTO;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project remove";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove a project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(token);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Delete:");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        serviceLocator.getProjectEndpoint().removeProject(token, projectId);
        System.out.println("The project has been deleted.");
    }

}
