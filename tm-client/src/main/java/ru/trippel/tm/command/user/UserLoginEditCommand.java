package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.UserDTO;
import ru.trippel.tm.command.AbstractCommand;

import java.io.IOException;

@NoArgsConstructor
public final class UserLoginEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user login edit";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws IOException, Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        @Nullable final UserDTO userTemp = serviceLocator.getUserEndpoint().findOneUser(token);
        if (userTemp == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Login.");
        @NotNull final String loginNew = serviceLocator.getTerminalService().read();
        userTemp.setLoginName(loginNew);
        serviceLocator.getUserEndpoint().updateUser(token, userTemp);
        serviceLocator.getSessionEndpoint().removeSession(token);
        serviceLocator.getStateService().setToken(null);
        System.out.println("Changes applied. Login required again.");
    }

}
