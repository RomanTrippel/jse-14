package ru.trippel.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.IStateService;
import ru.trippel.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class StateService implements IStateService {

    @Getter
    @Setter
    @Nullable
    private String token;

    @NotNull
    private final Map<String, AbstractCommand> commands;

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
