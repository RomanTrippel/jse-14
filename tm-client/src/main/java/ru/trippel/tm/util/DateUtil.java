package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor
public final class DateUtil {

    @NotNull
    private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Nullable
    public static XMLGregorianCalendar parseDate(@NotNull final String date) {
        try {
            @NotNull final Date dateSimple = simpleDateFormat.parse(date);
            @NotNull final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(dateSimple.getTime());
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (@NotNull final ParseException | DatatypeConfigurationException e) {
            return null;
        }

    }

    @NotNull
    public static String dateFormat(@NotNull final Date date) {
        return simpleDateFormat.format(date);

    }

}