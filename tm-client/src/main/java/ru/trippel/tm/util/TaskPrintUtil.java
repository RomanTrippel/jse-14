package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.endpoint.TaskDTO;

@NoArgsConstructor
public final class TaskPrintUtil {

    @NotNull
    public static String print(@NotNull final TaskDTO task) {
        @NotNull final String name = task.getName();
        @NotNull final String id = task.getId();
        @NotNull final String description = task.getDescription();
        @NotNull final String dateStart = task.getDateStart().toString();
        @NotNull final String dateFinish = task.getDateFinish().toString();
        @NotNull final String status = task.getStatus().toString();
        @NotNull final String dateCreate = task.getDateCreate().toString();
        return String.format("Task - %s, id - %s, description - %s, dateStart - %s, dateFinish - %s, status - %s," +
                " dateCreate - %s", name, id, description, dateStart, dateFinish, status, dateCreate);
    }

}
