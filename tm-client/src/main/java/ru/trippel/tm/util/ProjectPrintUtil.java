package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.endpoint.ProjectDTO;

@NoArgsConstructor
public final class ProjectPrintUtil {

    @NotNull
    public static String print(@NotNull final ProjectDTO projectDTO) {
        @NotNull final String name = projectDTO.getName();
        @NotNull final String id = projectDTO.getId();
        @NotNull final String description = projectDTO.getDescription();
        @NotNull final String dateStart = projectDTO.getDateStart().toString();
        @NotNull final String dateFinish = projectDTO.getDateFinish().toString();
        @NotNull final String status = projectDTO.getStatus().toString();
        @NotNull final String dateCreate = projectDTO.getDateCreate().toString();
        return String.format("Project - %s, id - %s, description - %s, dateStart - %s, dateFinish - %s, status - %s," +
                        " dateCreate - %s", name, id, description, dateStart, dateFinish, status, dateCreate);
    }

}
