package ru.trippel.tm;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.context.Bootstrap;

@NoArgsConstructor
public final class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}