package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

public interface IStateService {

    @Nullable
    String getToken();

    void setToken(@Nullable final String token);

    @NotNull
    List<AbstractCommand> getCommands();

}
