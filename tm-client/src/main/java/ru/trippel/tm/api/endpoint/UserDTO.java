
package ru.trippel.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.api.tm.trippel.ru/}abstractEntityDTO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="loginName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="projectSortingMethod" type="{http://endpoint.api.tm.trippel.ru/}sortingMethod" minOccurs="0"/&gt;
 *         &lt;element name="taskSortingMethod" type="{http://endpoint.api.tm.trippel.ru/}sortingMethod" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userDTO", propOrder = {
    "loginName",
    "password",
    "projectSortingMethod",
    "taskSortingMethod"
})
public class UserDTO
    extends AbstractEntityDTO
{

    protected String loginName;
    protected String password;
    @XmlSchemaType(name = "string")
    protected SortingMethod projectSortingMethod;
    @XmlSchemaType(name = "string")
    protected SortingMethod taskSortingMethod;

    /**
     * Gets the value of the loginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Sets the value of the loginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginName(String value) {
        this.loginName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the projectSortingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link SortingMethod }
     *     
     */
    public SortingMethod getProjectSortingMethod() {
        return projectSortingMethod;
    }

    /**
     * Sets the value of the projectSortingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortingMethod }
     *     
     */
    public void setProjectSortingMethod(SortingMethod value) {
        this.projectSortingMethod = value;
    }

    /**
     * Gets the value of the taskSortingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link SortingMethod }
     *     
     */
    public SortingMethod getTaskSortingMethod() {
        return taskSortingMethod;
    }

    /**
     * Sets the value of the taskSortingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortingMethod }
     *     
     */
    public void setTaskSortingMethod(SortingMethod value) {
        this.taskSortingMethod = value;
    }

}
