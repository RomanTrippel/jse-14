package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;
import java.util.List;

public class UserEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Nullable
    private static UserDTO userTest;

    @Nullable
    private static String tokenUser;

    @Test
    public void createUserWithPassword() throws Exception_Exception {
        userTest = userEndpoint.createUser("test10", "test");
        Assert.assertNotEquals(null, userTest);
        tokenUser = sessionEndpoint.createSession("test10", "test");
        userEndpoint.removeUser(tokenUser, userTest.getId());
    }

    @Test(expected = Exception.class)
    public void createUserWithoutName() throws Exception_Exception {
        userTest = userEndpoint.createUser("", "test");
        tokenUser = sessionEndpoint.createSession("", "test");
        userEndpoint.removeUser(tokenUser, userTest.getId());
        sessionEndpoint.removeSession(tokenUser);
    }

    @Test()
    public void removeUser() throws Exception_Exception {
        userTest = userEndpoint.createUser("test2", "test");
        tokenUser = sessionEndpoint.createSession("test2", "test");
        userEndpoint.removeUser(tokenUser, userTest.getId());
    }

    @Test()
    public void findAllUser() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("admin", "admin");
        @Nullable final List<UserDTO> result = userEndpoint.findAllUser(tokenUser);
        Assert.assertNotEquals(0, result.size());
        sessionEndpoint.removeSession(tokenUser);
    }

    @Test()
    public void findOneUser() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("admin", "admin");
        @Nullable final UserDTO result = userEndpoint.findOneUser(tokenUser);
        Assert.assertEquals("admin", result.getLoginName());
        sessionEndpoint.removeSession(tokenUser);
    }

    @Test()
    public void updateUser() throws Exception_Exception {
        userTest = userEndpoint.createUser("test5", "test");
        tokenUser = sessionEndpoint.createSession("test5", "test");
        userTest.setLoginName("testNew");
        userEndpoint.updateUser(tokenUser, userTest);
        tokenUser = sessionEndpoint.createSession("testNew", "test");
        Assert.assertEquals("testNew", userTest.getLoginName());
        userEndpoint.removeUser(tokenUser, userTest.getId());
    }

}
