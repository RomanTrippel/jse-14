package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.trippel.tm.api.endpoint.*;

public class SessionEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private static UserDTO userTest;

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenRemove;

    {
        try {
            tokenRemove = sessionEndpoint.createSession("test", "test");
        } catch (@NotNull final Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createSessionWithPassword() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("test", "test");
        Assert.assertNotEquals(null, tokenUser);
    }

    @Test
    public void removeAllSession() throws Exception_Exception {
        sessionEndpoint.removeSession(tokenUser);
    }

}
