package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;
import java.util.List;

public class TaskEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenAdmin;

    @NotNull
    private static final TaskDTO testTask = new TaskDTO();

    @Nullable
    private static final String projectId = "fad426c9-2c40-42fc-9391-20abc78ccf73";

    @BeforeClass
    public static void addUser() throws Exception {
        tokenUser = sessionEndpoint.createSession("user", "user");
        tokenAdmin = sessionEndpoint.createSession("admin", "admin");
    }

    @Test
    public void createTask() throws Exception_Exception {
        testTask.setName("TestTask");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final String taskName = taskEndpoint.findAllTasksByUserId(tokenUser).get(0).getName();
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        Assert.assertEquals("TestTask", taskName);
        taskEndpoint.removeTask(tokenUser, taskId);
    }

    @Test
    public void removeTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        taskEndpoint.removeTask(tokenUser, taskId);
        @Nullable final List<TaskDTO> task = taskEndpoint.findAllTask(tokenUser);
        Assert.assertEquals(0 , task.size());
    }

    @Test
    public void clearTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        taskEndpoint.clearTasks(tokenUser);
        @Nullable final int size = taskEndpoint.findAllTask(tokenUser).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void removeAllTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        testTask.setName("TestTaskAdmins");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenAdmin, testTask);
        taskEndpoint.removeAllTasks(tokenAdmin);
        @Nullable final int size = taskEndpoint.findAllTask(tokenAdmin).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void findAllTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        testTask.setName("TestTaskForRemove2");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final int size = taskEndpoint.findAllTask(tokenAdmin).size();
        Assert.assertEquals(2, size);
        taskEndpoint.removeAllTasks(tokenAdmin);
    }

    @Test
    public void findOneTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(tokenUser, taskId);
        Assert.assertEquals("TestTaskForRemove", task.getName());
        taskEndpoint.removeAllTasks(tokenAdmin);
    }

    @Test
    public void updateTask() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final TaskDTO task = taskEndpoint.findAllTasksByUserId(tokenUser).get(0);
        @NotNull final String taskId = task.getId();
        task.setDescription("new");
        taskEndpoint.updateTask(tokenUser, task);
        @Nullable final TaskDTO task1 = taskEndpoint.findOneTask(tokenUser, taskId);
        Assert.assertEquals("new", task1.getDescription());
        taskEndpoint.clearTasks(tokenUser);
    }

    @Test
    public void findAllTaskByUserId() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        testTask.setName("TestTaskForRemove2");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        @Nullable final int size = taskEndpoint.findAllTasksByUserId(tokenUser).size();
        Assert.assertEquals(2, size);
        taskEndpoint.clearTasks(tokenUser);
    }

    @Test
    public void findTaskByPart() throws Exception_Exception {
        testTask.setName("TestTaskForRemove");
        testTask.setProjectId(projectId);
        taskEndpoint.createTask(tokenUser, testTask);
        testTask.setName("TestTaskForRemove2");
        testTask.setProjectId(projectId);
        @Nullable final String taskName =
                taskEndpoint.findTaskByPart(tokenUser, "TestTaskForRemove").get(0).getName();
        Assert.assertEquals("TestTaskForRemove", taskName);
        taskEndpoint.clearTasks(tokenUser);
    }

}
