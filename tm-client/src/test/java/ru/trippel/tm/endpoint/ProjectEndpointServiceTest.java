package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;
import java.util.List;

public class ProjectEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private static UserDTO userTest;

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenAdmin;

    @NotNull
    private final static ProjectDTO testProject = new ProjectDTO();

    @BeforeClass
    public static void addUser() throws Exception {
        tokenUser = sessionEndpoint.createSession("user", "user");
        tokenAdmin = sessionEndpoint.createSession("admin", "admin");
    }

    @Test
    public void createProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final String projectName = projectEndpoint.findAllProjectsByUserId(tokenUser).get(0).getName();
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        Assert.assertEquals("TestProject", projectName);
        projectEndpoint.removeProject(tokenUser, projectId);
    }

    @Test
    public void removeProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        projectEndpoint.removeProject(tokenUser, projectId);
        @Nullable final List<ProjectDTO> project = projectEndpoint.findAllProject(tokenUser);
        Assert.assertEquals(0, project.size());
    }

    @Test
    public void clearProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        projectEndpoint.clearProjects(tokenUser);
        @Nullable final int size = projectEndpoint.findAllProject(tokenUser).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void removeAllProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        projectEndpoint.clearProjects(tokenUser);
        projectEndpoint.removeAllProjects(tokenAdmin);
        @Nullable final int size = projectEndpoint.findAllProject(tokenAdmin).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void findAllProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        projectEndpoint.clearProjects(tokenUser);
        @Nullable final int size = projectEndpoint.findAllProject(tokenAdmin).size();
        Assert.assertEquals(0, size);
        projectEndpoint.removeAllProjects(tokenAdmin);
    }

    @Test
    public void findOneProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(tokenUser, projectId);
        Assert.assertEquals("TestProject", project.getName());
        projectEndpoint.removeAllProjects(tokenAdmin);
    }

    @Test
    public void updateProject() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final ProjectDTO project = projectEndpoint.findAllProjectsByUserId(tokenUser).get(0);
        @NotNull final String projectId = project.getId();
        project.setDescription("new");
        projectEndpoint.updateProject(tokenUser, project);
        @Nullable final ProjectDTO project1 = projectEndpoint.findOneProject(tokenUser, projectId);
        Assert.assertEquals("new", project1.getDescription());
        projectEndpoint.clearProjects(tokenUser);
    }

    @Test
    public void findAllProjectByUserId() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final int size = projectEndpoint.findAllProjectsByUserId(tokenUser).size();
        Assert.assertEquals(1, size);
        projectEndpoint.clearProjects(tokenUser);
    }

    @Test
    public void findProjectByPart() throws Exception_Exception {
        testProject.setName("TestProject");
        projectEndpoint.createProject(tokenUser, testProject);
        @Nullable final String projectName =
                projectEndpoint.findProjectByPart(tokenUser, "TestProject").get(0).getName();
        Assert.assertEquals("TestProject", projectName);
        projectEndpoint.clearProjects(tokenUser);
    }

    @AfterClass
    public static void afterTest() throws Exception {
        testProject.setName("testProject");
        testProject.setId("fad426c9-2c40-42fc-9391-20abc78ccf73");
        projectEndpoint.createProject(tokenUser, testProject);
    }

}
