package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.util.ComparatorUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final Bootstrap bootstrap;

    public TaskService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> result = taskRepository.findAll();
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAllByComparator(@Nullable final String userId, @NotNull final SortingMethod sortingMethod) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final String sort = ComparatorUtil.getComparator(sortingMethod);
        @Nullable final List<Task> result = taskRepository.findAllByComparator(userId,sort);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAllById(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> result = taskRepository.findAllById(userId);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Task> findByPart(@Nullable final String userId, @NotNull final String searchText) {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> result = taskRepository.findByPart(userId, searchText);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> result = taskRepository.findAllByProjectId(userId, id);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final Task result = taskRepository.findOne(id);
        entityManager.close();
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.clear(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null) return;
        if (id.isEmpty()) return;
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

}
