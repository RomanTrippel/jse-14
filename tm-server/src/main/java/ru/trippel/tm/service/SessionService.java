package ru.trippel.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.api.service.ISessionService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.repository.SessionRepository;
import ru.trippel.tm.util.EncryptionSessionUtil;
import ru.trippel.tm.util.PropertyUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final Bootstrap bootstrap;

    public SessionService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Session persist(@Nullable final Session session) {
        if (session == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.persist(session);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null) return;
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable final Session session = sessionRepository.findOne(id);
        entityManager.close();
        return session;
    }

    @Nullable
    @Override
    public List<Session> findAllById(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable final List<Session> sessionList = sessionRepository.findAllById(userId);
        entityManager.close();
        return sessionList;
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        return null;
    }

    @Nullable
    @Override
    public Session merge(@Nullable final Session session) {
        return null;
    }


    @Override
    public void removeAll() {
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO, @NotNull TypeRole role) throws Exception {
        if (sessionDTO == null) return;
        if (!role.equals(sessionDTO.getRole())) throw new Exception("Not enough rights. You must be an admin.");
        validate(sessionDTO);
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new Exception("You must log in.");
        if (sessionDTO.getUserId() == null ||
                sessionDTO.getSignature() == null ||
                sessionDTO.getRole() == null) throw new Exception("You must log in.");
        @Nullable final Session sessionInBase = findOne(sessionDTO.getId());
        if (sessionInBase == null) throw new Exception("No session.");
        @Nullable final String signature = sessionDTO.getSignature();
        @Nullable final String signatureInBase = sessionInBase.getSignature();
        if (signatureInBase == null) throw new Exception("You must log in.");
        if (!(signatureInBase.equals(signature))) throw new Exception("You must log in.");
        final long passedTime = System.currentTimeMillis() - sessionInBase.getCreateDate();
        if (passedTime > 10*60*1000) {
            remove(sessionDTO.getId());
            throw new Exception("The session time of 10 minutes has expired. You need to log in.");
        }
    }

    @Nullable
    public String getToken(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(sessionDTO);
        return EncryptionSessionUtil.encrypt(json, PropertyUtil.encryptKey());
    }

    @Nullable
    public SessionDTO getSessionDTO(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        @NotNull String json = EncryptionSessionUtil.decrypt(token, PropertyUtil.encryptKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO sessionDTO = objectMapper.readValue(json, SessionDTO.class);
        return sessionDTO;
    }

}
