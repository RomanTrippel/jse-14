package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final Bootstrap bootstrap;

    public UserService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public User persist(@Nullable final User user) {
        if (user == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public List<User> findAll() {
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final List<User> result = userRepository.findAll();
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User result = userRepository.findOne(id);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public User findByLoginName(@Nullable final String loginName) {
        if (loginName == null) return null;
        if (loginName.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User result = userRepository.findByLoginName(loginName);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) {
        if (user == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (bootstrap.getEntityManagerFactory() == null) return;
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean checkLogin(@Nullable final String name) {
        if (name == null) return false;
        if (name.isEmpty()) return false;
        @NotNull final User user = findByLoginName(name);
        if (user == null) return false;
        return user.getLoginName().equals(name);
    }

}
