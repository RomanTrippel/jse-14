package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.util.ComparatorUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final Bootstrap bootstrap;

    public ProjectService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<Project> result = projectRepository.findAll();
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAllByComparator(@Nullable final String userId, @NotNull final SortingMethod sortingMethod){
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final String sort = ComparatorUtil.getComparator(sortingMethod);
        @Nullable final List<Project> result = projectRepository.findAllByComparator(userId,sort);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAllById(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<Project> result = projectRepository.findAllById(userId);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public List<Project> findByPart(@Nullable final String userId, @NotNull final String searchText) {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<Project> result = projectRepository.findByPart(userId, searchText);
        entityManager.close();
        return result;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable Project result = projectRepository.findOne(id);
        entityManager.close();
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.clear(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null) return;
        if (id.isEmpty()) return;
        if (bootstrap.getEntityManagerFactory() == null) return;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        if (bootstrap.getEntityManagerFactory() == null) return null;
        @NotNull final EntityManager entityManager = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

}
