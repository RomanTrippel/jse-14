package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.enumeration.SortingMethod;

@NoArgsConstructor
public final class ComparatorUtil {

    @NotNull
    public static String getComparator(@NotNull final SortingMethod sortingMethod){
        switch (sortingMethod) {
            case CREATION_ORDER:
                return "dateCreate";
            case START_DATE:
                return "dateStart";
            case FINISH_DATE:
                return "dateFinish";
            case STATUS:
                return "status";
        }
        return "dateCreate";
    }

}
