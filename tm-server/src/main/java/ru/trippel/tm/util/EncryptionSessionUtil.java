package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

@NoArgsConstructor
public class EncryptionSessionUtil {

    @Nullable
    private static SecretKeySpec secretKey;

    @Nullable
    private static byte[] key;

    public static void setKey(@NotNull final String myKey) throws Exception {
        @Nullable final MessageDigest sha;
        key = myKey.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKey = new SecretKeySpec(key, "AES");
    }

    @NotNull
    public static String encrypt(
            @NotNull final String strToEncrypt,
            @NotNull final String secret
    ) throws Exception {
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final byte[] data = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(data);
    }

    @NotNull
    public static String decrypt(
            @NotNull final String strToDecrypt,
            @NotNull final String secret
    )throws Exception {
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        final byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }

}
