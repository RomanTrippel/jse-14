package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.IProjectEndpoint;
import ru.trippel.tm.dto.ProjectDTO;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO createProject(@WebParam(name = "token") @NotNull final String token,
                                    @WebParam(name = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final ProjectDTO resultProjectDTO = projectDTO;
        resultProjectDTO.setUserId(sessionDTO.getUserId());
        return serviceLocator.getProjectService().persist(resultProjectDTO.getProject(serviceLocator)).getProjectDTO();
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsBySort(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (userId == null || user == null) return null;
        @NotNull final SortingMethod sortingMethod = user.getProjectSortingMethod();
        @Nullable final List<Project> projectList =
                serviceLocator.getProjectService().findAllByComparator(userId, sortingMethod);
        if (projectList == null) return null;
        @NotNull final List<ProjectDTO> resultProjectDTOList = new ArrayList<>();
        for (@NotNull final Project project: projectList) {
            resultProjectDTOList.add(project.getProjectDTO());
        }
        return resultProjectDTOList;
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().clear(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public final List<ProjectDTO> findAllProject(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findAll();
        if (projectList == null) return null;
        @NotNull final List<ProjectDTO> resultProjectDTOList = new ArrayList<>();
        for (@NotNull final Project project: projectList) {
            resultProjectDTOList.add(project.getProjectDTO());
        }
        return resultProjectDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findOneProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().findOne(projectId).getProjectDTO();
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO updateProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "projectDTO") @NotNull final ProjectDTO projectDTO
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final ProjectDTO projectDTOTemp = projectDTO;
        projectDTOTemp.setUserId(sessionDTO.getUserId());
        return serviceLocator.getProjectService().merge(projectDTOTemp.getProject(serviceLocator)).getProjectDTO();
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().remove(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findAllById(sessionDTO.getUserId());
        if (projectList == null) return null;
        @NotNull final List<ProjectDTO> resultProjectDTOList = new ArrayList<>();
        for (@NotNull final Project project: projectList) {
            resultProjectDTOList.add(project.getProjectDTO());
        }
        return resultProjectDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> findProjectByPart(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "searchPhrase") @NotNull final String searchText
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final List<Project> projectList =
                serviceLocator.getProjectService().findByPart(sessionDTO.getUserId(), searchText);
        if (projectList == null) return null;
        @NotNull final List<ProjectDTO> resultProjectDTOList = new ArrayList<>();
        for (@NotNull final Project project: projectList) {
            resultProjectDTOList.add(project.getProjectDTO());
        }
        return resultProjectDTOList;
    }

}
