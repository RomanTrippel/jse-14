package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.ISessionEndpoint;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.PasswordHashUtil;
import ru.trippel.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public String createSession(
            @WebParam(name = "loginName") @NotNull final String loginName,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final User user = serviceLocator.getUserService().findByLoginName(loginName);
        if (user == null) throw new Exception("Wrong login.");
        @Nullable final String hash = PasswordHashUtil.getHash(password);
        if (hash == null) throw new Exception("Error calculating hash.");
        if (!hash.equals(user.getPassword())) throw new Exception("Wrong pass");
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRole(user.getRole());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO));
        serviceLocator.getSessionService().persist(sessionDTO.getSession(serviceLocator));
        return serviceLocator.getSessionService().getToken(sessionDTO);
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "session") final String token) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().remove(sessionDTO.getId());
    }

}
