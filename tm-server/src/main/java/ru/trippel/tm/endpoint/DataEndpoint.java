package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.IDataEndpoint;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.enumeration.TypeRole;

import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.IDataEndpoint")
public class DataEndpoint implements IDataEndpoint {

    private IServiceLocator serviceLocator;

    public DataEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void dataSerializationSave(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataSerializationSave();
    }

    @Override
    public void dataFasterxmlXmlSave(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataFasterxmlXmlSave();
    }

    @Override
    public void dataFasterxmlJsonSave(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataFasterxmlJsonSave();
    }

    @Override
    public void dataJaxbXmlSave(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataJaxbXmlSave();
    }

    @Override
    public void dataJaxbJsonSave(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataJaxbJsonSave();
    }

    @Override
    public void dataSerializationLoad(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataSerializationLoad();
    }

    @Override
    public void dataFasterxmlXmlLoad(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataFasterxmlXmlLoad();
    }

    @Override
    public void dataFasterxmlJsonLoad(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataFasterxmlJsonLoad();
    }

    @Override
    public void dataJaxbXmlLoad(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataJaxbXmlLoad();
    }

    @Override
    public void dataJaxbJsonLoad(
            @WebParam(name = "session") @Nullable final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getDataService().dataJaxbJsonLoad();
    }

}
