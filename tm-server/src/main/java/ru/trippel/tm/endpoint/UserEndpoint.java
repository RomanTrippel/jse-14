package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.IUserEndpoint;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.dto.UserDTO;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.util.PasswordHashUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO createUser(
                           @WebParam(name = "loginName") @NotNull final String loginName,
                           @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLoginName(loginName);
        user.setPassword(PasswordHashUtil.getHash(password));
        return Objects.requireNonNull(serviceLocator.getUserService().persist(user)).getUserDTO();
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        serviceLocator.getUserService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDTO> findAllUser(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO, TypeRole.ADMIN);
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        if (userList == null) return null;
        @NotNull final List<UserDTO> resultUserDTOList = new ArrayList<>();
        for (@NotNull final User user: userList) {
            resultUserDTOList.add(user.getUserDTO());
        }
        return resultUserDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findOneUser(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getUserService().findOne(sessionDTO.getUserId()).getUserDTO();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findByLoginNameUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getUserService().findByLoginName(loginName).getUserDTO();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO updateUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "user") @NotNull final UserDTO userDTO
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final UserDTO resultUserDTO = userDTO;
        @Nullable final User user = serviceLocator.getUserService().findOne(userDTO.getId());
        if (user == null) return null;
        userDTO.setPassword(user.getPassword());
        return serviceLocator.getUserService().merge(resultUserDTO.getUser(serviceLocator)).getUserDTO();
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public boolean checkLoginUser(
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception {
        return serviceLocator.getUserService().checkLogin(loginName);
    }

}
