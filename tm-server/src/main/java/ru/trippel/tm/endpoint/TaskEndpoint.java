package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.ITaskEndpoint;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.dto.TaskDTO;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO createTask(@WebParam(name = "token") @NotNull final String token,
                           @WebParam(name = "taskDTO") @NotNull final TaskDTO taskDTO
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final TaskDTO resultTaskDTO = taskDTO;
        resultTaskDTO.setUserId(sessionDTO.getUserId());
        return serviceLocator.getTaskService().persist(resultTaskDTO.getTask(serviceLocator)).getTaskDTO();
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().clear(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllTask(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll();
        if (taskList == null) return null;
        @NotNull final List<TaskDTO> resultTaskDTOList = new ArrayList<>();
        for (@NotNull final Task task: taskList) {
            resultTaskDTOList.add(task.getTaskDTO());
        }
        return resultTaskDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findOneTask(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findOne(taskId).getTaskDTO();
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO updateTask(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "taskDTO") @NotNull final TaskDTO taskDTO
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final TaskDTO taskTemp = taskDTO;
        taskTemp.setUserId(sessionDTO.getUserId());
        return serviceLocator.getTaskService().merge(taskTemp.getTask(serviceLocator)).getTaskDTO();
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().remove(taskId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksBySort(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (userId == null || user == null) return null;
        @NotNull final SortingMethod sortingMethod = user.getTaskSortingMethod();
        @Nullable final List<Task> taskList =
                serviceLocator.getTaskService().findAllByComparator(userId, sortingMethod);
        if (taskList == null) return null;
        @NotNull final List<TaskDTO> resultTaskDTOList = new ArrayList<>();
        for (@NotNull final Task task: taskList) {
            resultTaskDTOList.add(task.getTaskDTO());
        }
        return resultTaskDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);
        @Nullable final List<Task> taskList =
                serviceLocator.getTaskService().findAllById(sessionDTO.getUserId());
        if (taskList == null) return null;
        @NotNull final List<TaskDTO> resultTaskDTOList = new ArrayList<>();
        for (@NotNull final Task task: taskList) {
            resultTaskDTOList.add(task.getTaskDTO());
        }
        return resultTaskDTOList;
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findTaskByPart(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "searchPhrase") @NotNull final String searchText
    ) throws Exception {
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO(token);
        serviceLocator.getSessionService().validate(sessionDTO);

        @Nullable final List<Task> taskList =
                serviceLocator.getTaskService().findByPart(sessionDTO.getUserId(), searchText);
        if (taskList == null) return null;
        @NotNull final List<TaskDTO> resultTaskDTOList = new ArrayList<>();
        for (@NotNull final Task task: taskList) {
            resultTaskDTOList.add(task.getTaskDTO());
        }
        return resultTaskDTOList;
    }

}
