package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.dto.SessionDTO;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity{

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    String signature = "";

    @NotNull
    private Long createDate = System.currentTimeMillis();

    @NotNull
    public SessionDTO getSessionDTO() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setId(this.id);
        session.setUserId(this.user.getId());
        session.setSignature(this.signature);
        session.setCreateDate(this.createDate);
        session.setRole(this.user.getRole());
        return session;
    }

}
