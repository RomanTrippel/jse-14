package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.entity.ISortable;
import ru.trippel.tm.dto.ProjectDTO;
import ru.trippel.tm.enumeration.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_project")
public final class Project extends AbstractEntity implements ISortable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", orphanRemoval = true)
    private List<Task> tasks;

    @NotNull
    public ProjectDTO getProjectDTO() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(this.id);
        project.setName(this.name);
        project.setDescription(this.description);
        project.setDateStart(this.dateStart);
        project.setDateFinish(this.dateFinish);
        project.setStatus(this.status);
        project.setDateCreate(this.dateCreate);
        project.setUserId(this.user.getId());
        return project;
    }

}