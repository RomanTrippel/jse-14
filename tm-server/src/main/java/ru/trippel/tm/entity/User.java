package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.dto.UserDTO;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.enumeration.TypeRole;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_user")
public final class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String loginName = "";

    @NotNull
    private String password = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private TypeRole role = TypeRole.USER;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SortingMethod projectSortingMethod = SortingMethod.CREATION_ORDER;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SortingMethod taskSortingMethod = SortingMethod.CREATION_ORDER;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Project> projects;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Session> session;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Task> tasks;

    @NotNull
    public UserDTO getUserDTO() {
        @NotNull final UserDTO user = new UserDTO();
        user.setId(this.id);
        user.setLoginName(this.loginName);
        user.setProjectSortingMethod(this.projectSortingMethod);
        user.setTaskSortingMethod(this.taskSortingMethod);
        return user;
    }

}
