package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.entity.ISortable;
import ru.trippel.tm.dto.TaskDTO;
import ru.trippel.tm.enumeration.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_task")
public final class Task extends AbstractEntity implements ISortable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    public TaskDTO getTaskDTO() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(this.id);
        task.setName(this.name);
        task.setDescription(this.description);
        task.setDateStart(this.dateStart);
        task.setDateFinish(this.dateFinish);
        task.setStatus(this.status);
        task.setDateCreate(this.dateCreate);
        task.setUserId(this.user.getId());
        task.setProjectId(this.project.getId());
        return task;
    }

}