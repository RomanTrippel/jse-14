package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    String createSession(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void removeSession(@WebParam(name = "session") String token) throws Exception;

}
