package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    void persist(@NotNull final Session session);

    void remove(@NotNull final String id);

    @Nullable
    Session findOne(@NotNull final String id);

    @Nullable
    List<Session> findAllById(@NotNull final String userId);

}
