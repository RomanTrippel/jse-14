package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    ProjectDTO createProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectDTO") @NotNull ProjectDTO projectDTO
    ) throws Exception;

    @WebMethod
    void clearProjects(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findAllProject(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO updateProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectDTO") @NotNull ProjectDTO projectDTO
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findAllProjectsBySort(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectByPart(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
