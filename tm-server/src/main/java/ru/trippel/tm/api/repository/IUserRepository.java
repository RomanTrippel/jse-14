package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    void persist(@NotNull final User user);

    @Nullable
    List<User> findAll();

    @Nullable
    User findOne(@NotNull final String id);

    @Nullable
    User findByLoginName(@NotNull final String login);

    void merge(@NotNull final User user);

    void removeOne(@NotNull final String id);

    void removeAll();

}
