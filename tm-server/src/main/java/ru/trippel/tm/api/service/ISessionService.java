package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.dto.SessionDTO;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void validate (@Nullable SessionDTO sessionDTO) throws Exception;

    void validate (@Nullable SessionDTO sessionDTO, @NotNull TypeRole role) throws Exception;

    @Nullable
    String getToken(@Nullable final SessionDTO sessionDTO) throws Exception;

    @Nullable
    SessionDTO getSessionDTO(@Nullable final String token) throws Exception;

    @Nullable
    List<Session> findAllById(@Nullable final String userId);

}
