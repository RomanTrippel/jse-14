package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskDTO createTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskDTO") @NotNull TaskDTO taskDTO
    ) throws Exception;

    @WebMethod
    void clearTasks(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    List<TaskDTO> findAllTask(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findOneTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO updateTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskDTO") @NotNull TaskDTO taskDTO
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTasksBySort(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findTaskByPart(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
