package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAllById(@Nullable final String userId);

    @Nullable
    List<Task> findAllByComparator(@Nullable final String userId, @NotNull SortingMethod sortingMethod);

    @Nullable
    Task findOne(@Nullable final String id);

    @Nullable
    List<Task> findByPart(@Nullable final String userId, @NotNull String searchText);

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    Task persist(@Nullable final Task task);

    @Nullable
    Task merge(@Nullable final Task task);

    void remove(@Nullable final String id);

    void clear(@Nullable final String userId);
}
