package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void persist(@NotNull final Task task);

    @Nullable
    List<Task> findAllByComparator(@NotNull final String userId, @NotNull String comparator);

    @Nullable
    List<Task> findAllById(@NotNull final String userId);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task findOne(@NotNull final String id);

    @Nullable
    List<Task> findByPart(@NotNull final String userId, @NotNull String searchText);

    void clear(@NotNull final String userId);

    void removeOne(@NotNull final String id);

    void removeAll();

    void merge(@NotNull final Task task);
}
