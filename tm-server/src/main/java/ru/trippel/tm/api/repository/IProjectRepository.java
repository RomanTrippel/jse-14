package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void persist(@NotNull Project project);

    @Nullable
    List<Project> findAllByComparator(@NotNull final String userId, @NotNull String comparator );

    @Nullable
    List<Project> findAllById(@NotNull final String userId);

    @Nullable
    List<Project> findAll();

    @Nullable
    Project findOne(@NotNull final String id);

    @Nullable
    List<Project> findByPart(@NotNull final String userId, @NotNull String searchText);

    void clear(@NotNull final String userId);

    void removeAll();

    void removeOne(@NotNull final String id);

    void merge(@NotNull Project project);

}
