package ru.trippel.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User findOne(@Nullable final String id);

    @Nullable
    User findByLoginName(@Nullable final String loginName);

    @Nullable
    User persist(@Nullable final User user);

    @Nullable
    User merge(@Nullable final User user);

    void remove(@Nullable final String id);

    boolean checkLogin(@Nullable final String name);

}
