package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO createUser(
            @WebParam(name = "loginName") @NotNull String loginName,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<UserDTO> findAllUser(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findOneUser(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findByLoginNameUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDTO updateUser(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "userDTO") @NotNull UserDTO userDTO
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "userId") @NotNull String userId
    ) throws Exception;

    @WebMethod
    boolean checkLoginUser(
            @WebParam(name = "loginName") @NotNull String loginName
    ) throws Exception;

}