package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAllById(@Nullable final String userId);

    @Nullable
    List<Project> findAllByComparator(@Nullable final String userId, @NotNull SortingMethod sortingMethod);

    @Nullable
    Project findOne(@Nullable final String id);

    @Nullable
    List<Project> findByPart(@Nullable final String userId, @NotNull String searchText);

    @Nullable
    Project persist(@Nullable Project project);

    @Nullable
    Project merge(@Nullable Project project);

    void remove(@Nullable final String id);

    void clear(@Nullable final String userId);

}
