package ru.trippel.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager entityManager;

    @Override
    public void persist(@NotNull final Task task) {
       entityManager.persist(task);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId",
                Task.class
        );
        query.setParameter("userId", userId);
        query.setParameter("projectId", projectId);
        return query.getResultList();    }

    @Nullable
    @Override
    public List<Task> findAllByComparator(@NotNull final String userId, @NotNull String comparator) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t." + comparator,
                Task.class
        );
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllById(@NotNull final String userId) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t FROM Task t WHERE t.user.id = :userId",
                Task.class
        );
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public List<Task> findByPart(@NotNull final String userId, @NotNull String searchText) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t FROM Task t WHERE t.user.id = :userId " +
                        "AND (t.name LIKE :searchText OR t.description LIKE :searchText)", Task.class
        );
        query.setParameter("userId", userId);
        query.setParameter("searchText", searchText);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t FROM Task t WHERE t.id=:id", Task.class
        );
        query.setParameter("id", id);
        @NotNull final List<Task> users = query.getResultList();
        return (users.isEmpty()) ? null : users.get(0);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<Task> taskList = findAllById(userId);
        if (taskList != null) {
            for (@NotNull final Task task : taskList)
                entityManager.remove(task);
        }
    }

    @Override
    public void removeAll() {
        @Nullable final List<Task> taskList = findAll();
        if (taskList != null) {
            for (@NotNull final Task task : taskList)
                entityManager.remove(task);
        }
    }

    @Override
    public void removeOne(@NotNull final String id) {
        entityManager.remove(findOne(id));
    }

    @Override
    public void merge(@NotNull final Task task) {
        entityManager.merge(task);
    }

}
