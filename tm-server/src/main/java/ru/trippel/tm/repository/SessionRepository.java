package ru.trippel.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;


    @Override
    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public Session findOne(@NotNull final String id) {
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(
                "SELECT s FROM Session s WHERE s.id=:id",
                Session.class
        );
        query.setParameter("id", id);
        @NotNull final List<Session> users = query.getResultList();
        return (users.isEmpty()) ? null : users.get(0);
    }

    @Override
    public void remove(@NotNull final String id) {
        entityManager.remove(findOne(id));
    }

    @Nullable
    @Override
    public List<Session> findAllById(@NotNull final String userId) {
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(
                "SELECT s FROM Session s WHERE s.user.id = :userId",
                Session.class
        );
        query.setParameter("userId", userId);
        return query.getResultList();
    }

}
