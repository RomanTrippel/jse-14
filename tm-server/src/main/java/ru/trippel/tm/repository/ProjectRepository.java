package ru.trippel.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    @Override
    public void persist(@NotNull Project project) {
       entityManager.persist(project);
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllByComparator(@NotNull final String userId, @NotNull String comparator) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p." + comparator,
                Project.class
        );
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllById(@NotNull final String userId) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.user.id = :userId",
                Project.class
        );
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public List<Project> findByPart(@NotNull final String userId, @NotNull String searchText) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.user.id = :userId " +
                        "AND (p.name LIKE :searchText OR p.description LIKE :searchText)", Project.class
        );
        query.setParameter("userId", userId);
        query.setParameter("searchText", searchText);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.id=:id", Project.class
        );
        query.setParameter("id", id);
        @NotNull final List<Project> users = query.getResultList();
        return (users.isEmpty()) ? null : users.get(0);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<Project> projectList = findAllById(userId);
        if (projectList != null) {
            for (@NotNull final Project project : projectList)
                entityManager.remove(project);
        }
    }

    @Override
    public void removeAll() {
        @Nullable final List<Project> projectList = findAll();
        if (projectList != null) {
            for (@NotNull final Project project : projectList)
                entityManager.remove(project);
        }
    }

    @Override
    public void removeOne(@NotNull final String id) {
        entityManager.remove(findOne(id));
    }

    @Override
    public void merge(@NotNull Project project) {
        entityManager.merge(project);
    }

}
