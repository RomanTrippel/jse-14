package ru.trippel.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RequiredArgsConstructor
public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    @Override
    public void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id){
        @NotNull final TypedQuery<User> query = entityManager.createQuery(
                "SELECT u FROM User u WHERE u.id=:id", User.class
        );
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Nullable
    @Override
    public User findByLoginName(@NotNull final String loginName){
        @NotNull final TypedQuery<User> query = entityManager.createQuery(
                "SELECT u FROM User u WHERE u.loginName=:loginName", User.class
        );
        query.setParameter("loginName", loginName);
        @NotNull final List<User> users = query.getResultList();
        return (users.isEmpty()) ? null : users.get(0);
    }

    @Override
    public void merge(@NotNull final User user) {
        entityManager.merge(user);
    }

    @Override
    public void removeOne(@NotNull final String id) {
     entityManager.remove(findOne(id));
    }

    @Override
    public void removeAll() {
        @Nullable final List<User> userList = findAll();
        if (userList != null) {
            for (@NotNull final User user : userList)
                entityManager.remove(user);
        }
    }

}
