package ru.trippel.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ISessionService;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO{

    @NotNull
    private String loginName = "";

    @NotNull
    private String password = "";

    @NotNull
    private SortingMethod projectSortingMethod = SortingMethod.CREATION_ORDER;

    @NotNull
    private SortingMethod taskSortingMethod = SortingMethod.CREATION_ORDER;

    @NotNull
    public User getUser(@NotNull final IServiceLocator serviceLocator) {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        @NotNull final User user = new User();
        user.setId(this.id);
        user.setLoginName(this.loginName);
        user.setPassword(this.password);
        user.setProjectSortingMethod(this.projectSortingMethod);
        user.setTaskSortingMethod(this.taskSortingMethod);
        user.setProjects(projectService.findAllById(this.id));
        user.setTasks(taskService.findAllById(this.id));
        user.setSession(sessionService.findAllById(this.id));
        return user;
    }

}
