package ru.trippel.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public class SessionDTO extends AbstractEntityDTO{

    @NotNull
    private String userId;

    @Nullable
    String signature = "";

    @NotNull
    private Long createDate = System.currentTimeMillis();

    @Nullable
    TypeRole role = TypeRole.USER;

    @NotNull
    public Session getSession(@NotNull final IServiceLocator serviceLocator) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final Session session = new Session();
        session.setId(this.id);
        session.setUser(userService.findOne(this.userId));
        session.setSignature(this.signature);
        session.setCreateDate(this.createDate);
        return session;
    }

}
