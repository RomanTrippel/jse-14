package ru.trippel.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.Status;

import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public class ProjectDTO extends AbstractEntityDTO{

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private String userId = "";

    @NotNull
    public Project getProject(@NotNull final IServiceLocator serviceLocator) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final Project project = new Project();
        project.setId(this.id);
        project.setName(this.name);
        project.setDescription(this.description);
        project.setDateStart(this.dateStart);
        project.setDateFinish(this.dateFinish);
        project.setUser(userService.findOne(this.userId));
        project.setStatus(this.status);
        project.setDateCreate(this.dateCreate);
        project.setTasks(taskService.findAllByProjectId(this.userId, this.id));
        return project;
    }

}
