package ru.trippel.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.Status;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public class TaskDTO extends AbstractEntityDTO{

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private String userId = "";

    @NotNull
    private String projectId = "";

    @NotNull
    public Task getTask(@NotNull final IServiceLocator serviceLocator) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final Task task = new Task();
        task.setId(this.id);
        task.setName(this.name);
        task.setDescription(this.description);
        task.setDateStart(this.dateStart);
        task.setDateFinish(this.dateFinish);
        task.setProject(projectService.findOne(this.projectId));
        task.setUser(userService.findOne(this.userId));
        task.setStatus(this.status);
        task.setDateCreate(this.dateCreate);
        return task;
    }

}
