package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.api.service.*;
import ru.trippel.tm.endpoint.*;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.*;
import ru.trippel.tm.util.PasswordHashUtil;
import ru.trippel.tm.util.PropertyUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final IUserService userService = new UserService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    public final ISubjectArea subjectAreaService = new SubjectAreaService();

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @Nullable
    private EntityManagerFactory entityManagerFactory;

    private void initDefaultUser() {
        if (!userService.checkLogin("admin")) {
            @NotNull final User admin = new User();
            admin.setLoginName("admin");
            admin.setPassword(PasswordHashUtil.getHash("admin"));
            admin.setRole(TypeRole.ADMIN);
            userService.persist(admin);
        }
        if (!userService.checkLogin("user")) {
            @NotNull final User user = new User();
        user.setLoginName("user");
        user.setPassword(PasswordHashUtil.getHash("user"));
        userService.persist(user);
        }
    }

    @NotNull
    private EntityManagerFactory initEntityManagerFactory() throws Exception {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, PropertyUtil.dbDriver());
        settings.put(org.hibernate.cfg.Environment.URL, PropertyUtil.dbHost());
        settings.put(org.hibernate.cfg.Environment.USER, PropertyUtil.dbLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, PropertyUtil.dbPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, PropertyUtil.dbDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();

    }

    public void start() {
        try {
            entityManagerFactory = initEntityManagerFactory();
        } catch (@NotNull final Exception e) {
            e.printStackTrace();
        }
        initDefaultUser();
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/DataEndpoint?wsdl", dataEndpoint);
        System.out.println("Sm-server started successfully.");
    }

}